FROM openjdk:8u151-jre-stretch
WORKDIR /app
COPY ./shopping.jar /app/
EXPOSE 8000
CMD java -server -Xms256m -Xmx256m -XX:+UseG1GC -jar shopping.jar
