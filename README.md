1. The app is running on 157.90.20.200:8000 

2. The Postman collection is in the root of project, you should obtain admin token(by admin username and password that I'v already created) via login 
 api and set it in a global variable in Postman, named "admin-token", in order to call admin endpoints. 

3. You should obtain user token(by credentials registered via register api) and set it in a global variable in Postman, named "user-token", in order to call user endpoints

4. Admin username: admin

5. Admin password: 123456

6. I also created two categories named "category 1" with id 1, and "category 2" with id 2
