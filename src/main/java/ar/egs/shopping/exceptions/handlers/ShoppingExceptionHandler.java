package ar.egs.shopping.exceptions.handlers;


import ar.egs.shopping.dto.ErrorResponse;
import ar.egs.shopping.exceptions.ProductNotFoundException;
import ar.egs.shopping.exceptions.UserNotFoundException;
import ar.egs.shopping.services.BaseService;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ShoppingExceptionHandler extends BaseService {

    @ExceptionHandler(value = {DataIntegrityViolationException.class})
    public ResponseEntity<Object> handleInvalidInputException(DataIntegrityViolationException ex) {
        String message = ex.getRootCause() != null ? getFieldNameFromUniqueException(ex.getRootCause().getMessage()) : "error";
        message = "duplicated." + message;
        return new ResponseEntity<>(new ErrorResponse("100", getErrorMessage(message)), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(value = {InvalidGrantException.class})
    public ResponseEntity<Object> InvalidGrantException(InvalidGrantException ex) {
        return new ResponseEntity<>(new ErrorResponse("101", getErrorMessage("bad.credentials")), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = {ProductNotFoundException.class})
    public ResponseEntity<Object> expiredRegistration(ProductNotFoundException ex) {
        return new ResponseEntity<>(new ErrorResponse("102", getErrorMessage("product.not.found")), HttpStatus.INTERNAL_SERVER_ERROR);
    }


    @ExceptionHandler(value = {UserNotFoundException.class})
    public ResponseEntity<Object> userNotFound(UserNotFoundException ex) {
        return new ResponseEntity<>(new ErrorResponse("103", getErrorMessage("user.not.found")), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private String getFieldNameFromUniqueException(String message) {
        message = message.substring(message.indexOf("(") + 1, message.indexOf(")"));
        return message;
    }

}
