package ar.egs.shopping.entities;

public enum Role {

    USER("user"), ADMIN("admin");

    Role(String name) {
        this.name = name;
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
