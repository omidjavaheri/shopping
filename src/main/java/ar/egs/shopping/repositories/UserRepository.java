package ar.egs.shopping.repositories;

import ar.egs.shopping.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {

    User findByUsername(String username);
    boolean existsByUsernameAndEnabledIsTrue(String username);
}
