package ar.egs.shopping.repositories;

import ar.egs.shopping.entities.Product;
import ar.egs.shopping.entities.ProductUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ProductUserRepository extends JpaRepository<ProductUser, Long> {

    ProductUser findByProductAndUser_Username(Product product, String username);
    @Query("select avg(pu.rate) from ProductUser pu where pu.user.username = :username and pu.product.id = :pId")
    Double calculateAve(@Param("username") String username, @Param("pId") Long pId);
}
