package ar.egs.shopping.controllers;

import ar.egs.shopping.entities.User;
import ar.egs.shopping.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/account/v1")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @PostMapping("/register")
    @ResponseBody
    public void register(@RequestBody User user) {
        accountService.register(user);
    }


}
