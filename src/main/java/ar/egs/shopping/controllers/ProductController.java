package ar.egs.shopping.controllers;

import ar.egs.shopping.dto.ProductSearchDTO;
import ar.egs.shopping.dto.RateOrCommentDTO;
import ar.egs.shopping.entities.Product;
import ar.egs.shopping.services.ProductService;
import ar.egs.shopping.services.ProductUserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user/v1")
public class ProductController {

    private final ProductService productService;
    private final ProductUserService productUserService;

    public ProductController(ProductService productService, ProductUserService productUserService) {
        this.productService = productService;
        this.productUserService = productUserService;
    }

    @PostMapping("product/search")
    @ResponseBody
    public List<Product> search(@RequestBody ProductSearchDTO productSearchDTO) {
        return this.productService.search(productSearchDTO);
    }

    @PostMapping("product/rate")
    @ResponseBody
    public void rate(@RequestBody RateOrCommentDTO rateOrCommentDTO) {
        this.productUserService.rate(rateOrCommentDTO);
    }

    @PostMapping("product/comment")
    @ResponseBody
    public void comment(@RequestBody RateOrCommentDTO rateOrCommentDTO) {
        this.productUserService.comment(rateOrCommentDTO);
    }

}
