package ar.egs.shopping.controllers;

import ar.egs.shopping.dto.BlockDTO;
import ar.egs.shopping.services.AdminService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin/v1")
public class UserAdminController {

    private final AdminService adminService;

    public UserAdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    @PostMapping("block")
    @ResponseBody
    public void blockOrUnblock(@RequestBody BlockDTO blockDTO) {
        adminService.blockOrUnblock(blockDTO);
    }

}
