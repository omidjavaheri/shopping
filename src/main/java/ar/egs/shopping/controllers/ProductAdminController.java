package ar.egs.shopping.controllers;

import ar.egs.shopping.dto.IdDTO;
import ar.egs.shopping.dto.PageableDTO;
import ar.egs.shopping.entities.Product;
import ar.egs.shopping.services.AdminService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin/v1")
public class ProductAdminController {

    private final AdminService adminService;

    public ProductAdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    @PostMapping("product/list")
    @ResponseBody
    public List<Product> products(@RequestBody PageableDTO pageableDTO) {
        return adminService.products(pageableDTO);
    }

    @PostMapping("product/save")
    @ResponseBody
    public Product products(@RequestBody Product product) {
        return adminService.saveOrUpdate(product);
    }

    @DeleteMapping("product/delete")
    @ResponseBody
    public void delete(@RequestBody IdDTO idDTO) {
        adminService.delete(idDTO);
    }


}
