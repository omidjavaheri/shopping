package ar.egs.shopping.services;

import ar.egs.shopping.dto.IdDTO;
import ar.egs.shopping.dto.PageableDTO;
import ar.egs.shopping.dto.ProductSearchDTO;
import ar.egs.shopping.entities.Product;
import ar.egs.shopping.exceptions.ProductNotFoundException;
import ar.egs.shopping.repositories.ProductRepository;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProductService extends BaseService {

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Product saveOrUpdate(Product product) {
        if (product.getId() != null) {
            Optional<Product> optionalProduct = productRepository.findById(product.getId());
            if (!optionalProduct.isPresent())
                throw new ProductNotFoundException();
            Product entity = optionalProduct.get();
            entity.setName(product.getName());
            entity.setRate(product.getRate());
            entity.setCategory(product.getCategory());
            entity.setPrice(product.getPrice());
            product = entity;
        }
        return productRepository.save(product);
    }

    public List<Product> products(PageableDTO pageableDTO) {
        Page<Product> products = productRepository.findAll(getPageable(pageableDTO));
        return products.getContent();
    }

    public void delete(IdDTO idDTO) {
        Optional<Product> product = productRepository.findById(idDTO.getId());
        if (!product.isPresent()) {
            throw new ProductNotFoundException();
        }
        productRepository.delete(product.get());
    }

    public List<Product> search(ProductSearchDTO productSearchDTO) {
        Page<Product> products = productRepository.search(productSearchDTO.getName(),
                productSearchDTO.getFromPrice(),
                productSearchDTO.getToPrice(),
                productSearchDTO.getRate(),
                getPageable(productSearchDTO.getPageableDTO()));
        return products.getContent();
    }

    public Product findById(Long id) {
        Optional<Product> product = productRepository.findById(id);
        if (!product.isPresent())
            throw new ProductNotFoundException();
        return product.get();
    }

}
