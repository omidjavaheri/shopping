package ar.egs.shopping.services;

import ar.egs.shopping.dto.PageableDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;

public class BaseService {
    @Autowired
    @Qualifier("egcMessageSource")
    private MessageSource resource;

    protected String getErrorMessage(String key) {
        return resource.getMessage(key, null, LocaleContextHolder.getLocale());
    }

    protected String getErrorMessage(String key, Object ...args) {
        return resource.getMessage(key, args, LocaleContextHolder.getLocale());
    }

    protected Pageable getPageable(PageableDTO dto) {
        Sort sort = Sort.by(Sort.Direction.ASC.toString().equals(dto.getDirection()) ? Sort.Direction.ASC : Sort.Direction.DESC, dto.getSortBy());
        return PageRequest.of(dto.getPage(), dto.getSize(), sort);
    }

    protected String getLoggedInUsername() {
        return SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
    }
}
