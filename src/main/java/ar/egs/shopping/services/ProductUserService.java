package ar.egs.shopping.services;

import ar.egs.shopping.dto.RateOrCommentDTO;
import ar.egs.shopping.entities.Product;
import ar.egs.shopping.entities.ProductUser;
import ar.egs.shopping.entities.User;
import ar.egs.shopping.exceptions.UserNotFoundException;
import ar.egs.shopping.repositories.ProductUserRepository;
import ar.egs.shopping.repositories.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ProductUserService extends BaseService {

    private final ProductUserRepository productUserRepository;
    private final ProductService productService;
    private final UserRepository userRepository;

    public ProductUserService(ProductUserRepository productUserRepository,
                              ProductService productService,
                              UserRepository userRepository) {
        this.productUserRepository = productUserRepository;
        this.productService = productService;
        this.userRepository = userRepository;
    }

    public void rate(RateOrCommentDTO rateOrCommentDTO) {
        Product product = productService.findById(rateOrCommentDTO.getProductId());
        ProductUser productUser = productUserRepository.findByProductAndUser_Username(product, getLoggedInUsername());
        if (productUser != null) {
            productUser.setRate(rateOrCommentDTO.getRate());
        } else {
            this.createProductUser(product, rateOrCommentDTO);
        }
        Double aveOfRates = productUserRepository.calculateAve(getLoggedInUsername(), product.getId());
        product.setRate(aveOfRates);
    }

    public void comment(RateOrCommentDTO rateOrCommentDTO) {
        Product product = productService.findById(rateOrCommentDTO.getProductId());
        ProductUser productUser = productUserRepository.findByProductAndUser_Username(product, getLoggedInUsername());
        if (productUser != null) {
            productUser.setComment(rateOrCommentDTO.getComment());
        } else {
            this.createProductUser(product, rateOrCommentDTO);
        }
    }

    private void createProductUser(Product product, RateOrCommentDTO rateOrCommentDTO) {
        ProductUser entity = new ProductUser();
        entity.setProduct(product);
        User user = userRepository.findByUsername(getLoggedInUsername());
        if (user == null)
            throw new UserNotFoundException();
        entity.setUser(user);
        entity.setRate(rateOrCommentDTO.getRate());
        if (rateOrCommentDTO.getComment() != null)
            entity.setComment(rateOrCommentDTO.getComment());
        if (rateOrCommentDTO.getRate() != null)
            entity.setRate(rateOrCommentDTO.getRate());
        productUserRepository.save(entity);
    }
}
