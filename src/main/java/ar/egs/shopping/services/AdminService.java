package ar.egs.shopping.services;

import ar.egs.shopping.dto.BlockDTO;
import ar.egs.shopping.dto.IdDTO;
import ar.egs.shopping.dto.PageableDTO;
import ar.egs.shopping.entities.Product;
import ar.egs.shopping.entities.User;
import ar.egs.shopping.exceptions.UserNotFoundException;
import ar.egs.shopping.repositories.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AdminService {

    private final UserRepository userRepository;
    private final ProductService productService;

    public AdminService(UserRepository userRepository, ProductService productService) {
        this.userRepository = userRepository;
        this.productService = productService;
    }

    public Product saveOrUpdate(Product product) {
        return productService.saveOrUpdate(product);
    }

    public List<Product> products(PageableDTO pageableDTO) {
        return productService.products(pageableDTO);
    }

    public void delete(IdDTO idDTO) {
        productService.delete(idDTO);
    }

    public void blockOrUnblock(BlockDTO blockDTO) {
        User user = userRepository.findByUsername(blockDTO.getUsername());
        if (user == null) {
            throw new UserNotFoundException();
        }
        user.setEnabled(blockDTO.isEnabled());
    }
}
