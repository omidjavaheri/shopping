package ar.egs.shopping.configuration;

import com.google.gson.Gson;
import ar.egs.shopping.dto.ErrorResponse;
import ar.egs.shopping.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class UserValidationFilter extends GenericFilterBean {

    private final UserRepository userRepository;
    private final MessageSource resource;

    public UserValidationFilter(UserRepository userRepository, @Qualifier("egcMessageSource") MessageSource resource) {
        this.userRepository = userRepository;
        this.resource = resource;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
                         FilterChain chain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req;
        if (request.getRequestURI().contains("user")) {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String username = (String) authentication.getPrincipal();
            boolean isEnabled = userRepository.existsByUsernameAndEnabledIsTrue(username);
            if (!isEnabled) {
                HttpServletResponse response = ((HttpServletResponse) res);
                response.setStatus(HttpStatus.UNAUTHORIZED.value());

                response.setContentType("application/json; charset=UTF-8");
                response.setCharacterEncoding("UTF-8");

                String message = resource.getMessage("user.has.been.disabled", null, LocaleContextHolder.getLocale());
                response.getWriter()
                        .println(new Gson().toJson(new ErrorResponse("105", message)));
                return;
            }
        }
        chain.doFilter(req, res);

    }

}
